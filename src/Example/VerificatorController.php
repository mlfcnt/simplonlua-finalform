<?php

namespace App\Controller;

use App\Entity\Owner;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use simplonlua\LuaCalculatorBundle\Verificator\Verificator;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Exemple de controller à constuire dans votre dossier src.

class VerificatorController extends Controller
{
    /**
     * @Route("/", name="verificator")
     */

    //  Cette fonction permet  : 
    // 1 - Fetch les données d'une entité puis les transformer au format Json
    // 2 - Compare ces données avec un fichier Json Schema présent dans votre App.
    // 3 - Si les données sont valides, alors un calcul va s'opérer. a complèter
    // 4 - A completer
    public function jsonValidator()
    {

        $encoder = [new JsonEncoder()];
        $normalizer = [new ObjectNormalizer()];

        // Dans cet exemple, serializer fetch la data d'une entité owner puis les transforme en un fichier json.

        $serializer = new Serializer($normalizer, $encoder);
        $owner = $this->getDoctrine()->getRepository(Owner::class)
                      ->findAll();
        $jsonContent = $serializer->serialize($owner, 'json');

        // Ici, nous avons décidé de placer le Json Schema dans un dossier docs placé à la racine de notre App. A vous de voir comment vous voulez structurer votre projet. 

        $webPath = $this->get('kernel')->getProjectDir() . '/docs/'; 

        $jsonSchema = realpath($webPath . 'jsonSchema.json');




        $validJson = $this->container->get('lua.verificator')->verificator($jsonContent, $jsonSchema);
        if ($validJson === true) {
            // $test = 'ok';

        } else {
        }
        return $this->render('validator/index.html.twig', [
            // 'verificator' => $verificator,

            
        ]);
    }
}
